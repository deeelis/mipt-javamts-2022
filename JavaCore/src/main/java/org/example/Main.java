package org.example;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


public class Main {
    public static void main(String[] args) {
        ConcurrentMap<String, ConcurrentMap <String, String>> map = new ConcurrentHashMap<>();
        ConcurrentMap<String, String> tmp = new ConcurrentHashMap<>();
        tmp.put("firstName", "Vasya");
        tmp.put("secondName", "Petrov");
        map.put("88005553535", tmp);
        String JSON = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"88005553535\"}";
        Message message = new Message();
        message.setContent(JSON);
        message.enrichmentType =    EnrichmentType.MSISDN;
        EnrichmentService service = new EnrichmentService(map);
        System.out.println(service.enrich(message));

    }
}