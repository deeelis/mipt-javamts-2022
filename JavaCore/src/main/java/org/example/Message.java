package org.example;


public class Message {
    private String content;
    public EnrichmentType enrichmentType;

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public Message(){}
    public Message(String contentValue, EnrichmentType enrichmentTypeValue){
        content = contentValue;
        enrichmentType = enrichmentTypeValue;
    }

}
