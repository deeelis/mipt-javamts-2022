package org.example;

import com.google.gson.Gson;
import com.google.gson.*;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

public class EnrichmentService {
        private final ConcurrentMap<String, ConcurrentMap<String, String>> Enrichments;
        EnrichmentService(ConcurrentMap<String, ConcurrentMap<String, String>> Enrichments) {
            this.Enrichments = Enrichments;
        }

        public String enrich(Message message){
            JsonObject jsonObject = JsonParser.parseString(message.getContent()).getAsJsonObject();
            if (message.enrichmentType == EnrichmentType.MSISDN) {
                message.setContent(find(jsonObject).toString());
            }
            return message.getContent();
        }
        public JsonObject find(JsonObject jsonObject) {
            String msisdn = String.valueOf(jsonObject.get("msisdn")).replace("\"", "");
            Map<String, String> info = this.Enrichments.get(msisdn);
            if (info != null) {
                JsonElement jsonMap = JsonParser.parseString((new Gson()).toJson(info));
                jsonObject.add("enrichment", jsonMap);
            }
            return jsonObject;
        }
    }
