package org.example;

import org.example.FoodTypes.*;
import org.example.Animals.*;

public class Main {
    public static void main(String[] args) {
        Camel camel = new Camel();
        Eagle eagle = new Eagle();
        Dolphin dolphin = new Dolphin();
        Horse horse = new Horse();
        Tiger tiger = new Tiger();

        camel.camelEat(FoodType.GRASS);
        camel.camelEat(FoodType.BEEF);
        camel.move();
        eagle.eagleEat(FoodType.MEAT);
        eagle.eagleEat(FoodType.FISH);
        eagle.move();
        dolphin.dolphinEat(FoodType.FISH);
        dolphin.dolphinEat(FoodType.GRASS);
        dolphin.move();
        horse.horseEat(FoodType.GRASS);
        horse.horseEat(FoodType.MEAT);
        horse.move();
        tiger.tigerEat(FoodType.BEEF);
        tiger.tigerEat(FoodType.FISH);
        tiger.move();
    }
}