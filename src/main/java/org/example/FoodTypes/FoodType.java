package org.example.FoodTypes;

public enum FoodType {
    BEEF,
    GRASS,
    MEAT,
    FISH
}