package org.example.AnimalTypes;

import org.example.Animals.Animal;

public abstract class Flying extends Animal {
    public void fly(String name) {
        System.out.println(name + ": I am flying!");
    }
}