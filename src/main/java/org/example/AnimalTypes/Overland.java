package org.example.AnimalTypes;

import org.example.Animals.Animal;

public abstract class Overland extends Animal{
    public void walk(String name) {
        System.out.println(name + ": I am walking!");
    }
}