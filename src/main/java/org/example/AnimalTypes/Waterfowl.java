package org.example.AnimalTypes;

import org.example.Animals.Animal;

public abstract class Waterfowl extends Animal {
    public void swim(String name) {
        System.out.println(name + ": I am swimming!");
    }
}