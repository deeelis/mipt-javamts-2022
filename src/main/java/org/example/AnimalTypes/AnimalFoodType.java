package org.example.AnimalTypes;

public enum AnimalFoodType {
    PREDATOR,
    HERBIVORE
}