package org.example.Animals;

import org.example.FoodTypes.*;
import org.example.AnimalTypes.*;

public class Camel extends Overland{
    String name = "Camel";
    FoodType foodType = FoodType.GRASS;
    AnimalFoodType animalFoodType = AnimalFoodType.HERBIVORE;

    public void camelEat(FoodType givenFoodType) {
        eat(name, givenFoodType, foodType);
    }

    public void move() {
        walk(name);
    }
}