package org.example.Animals;

import org.example.FoodTypes.*;
import org.example.AnimalTypes.*;

public class Horse extends Overland {
    String name = "Horse";
    FoodType foodType = FoodType.GRASS;
    AnimalFoodType animalFoodType = AnimalFoodType.HERBIVORE;

    public void horseEat(FoodType givenFoodType) {
        eat(name, givenFoodType, foodType);
    }

    public void move() {
        walk(name);
    }
}