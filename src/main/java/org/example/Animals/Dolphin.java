package org.example.Animals;

import org.example.FoodTypes.*;
import org.example.AnimalTypes.*;

public class Dolphin extends Waterfowl {
    String name = "Dolphin";
    FoodType foodType = FoodType.FISH;
    AnimalFoodType animalFoodType = AnimalFoodType.PREDATOR;

    public void dolphinEat(FoodType givenFoodType) {
        eat(name, givenFoodType, foodType);
    }

    public void move() {
        swim(name);
    }
}