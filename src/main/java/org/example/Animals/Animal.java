package org.example.Animals;

import org.example.FoodTypes.*;

public abstract class Animal {
    public void eat(String name, FoodType food_type, FoodType permitted_type) {
        if (food_type == permitted_type) {
            System.out.println(name + ": So delicious!");
        } else {
            System.out.println(name + ": Ugh, awful!");
        }
    }
}