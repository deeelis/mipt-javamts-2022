package org.example.Animals;

import org.example.FoodTypes.*;
import org.example.AnimalTypes.*;

public class Tiger extends Overland{
    String name = "Tiger";
    FoodType foodType = FoodType.BEEF;
    AnimalFoodType animalFoodType = AnimalFoodType.PREDATOR;

    public void tigerEat(FoodType givenFoodType) {
        eat(name, givenFoodType, foodType);
    }

    public void move() {
        walk(name);
    }
}