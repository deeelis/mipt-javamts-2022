package org.example.Animals;

import org.example.FoodTypes.*;
import org.example.AnimalTypes.*;

public class Eagle extends Flying {
    String name = "Eagle";
    FoodType foodType = FoodType.MEAT;
    AnimalFoodType animalFoodType = AnimalFoodType.PREDATOR;

    public void eagleEat(FoodType givenFoodType) {
        eat(name, givenFoodType, foodType);
    }

    public void move() {
        fly(name);
    }
}